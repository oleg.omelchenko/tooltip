import React, { CSSProperties } from 'react';
interface ChipProps {
    size?: number;
    color: string;
    style?: CSSProperties;
}
export declare const Chip: React.NamedExoticComponent<ChipProps>;
export {};
//# sourceMappingURL=Chip.d.ts.map