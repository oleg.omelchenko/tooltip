/// <reference types="react" />
import { TooltipStateContextData, TooltipStateContextDataVisible } from './context';
export declare const isVisibleTooltipState: (state: TooltipStateContextData) => state is TooltipStateContextDataVisible;
export declare const Tooltip: () => JSX.Element | null;
//# sourceMappingURL=Tooltip.d.ts.map