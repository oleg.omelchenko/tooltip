import React from 'react';
import { TooltipStateContextDataVisible } from './context';
interface TooltipWrapperProps {
    position: TooltipStateContextDataVisible['position'];
    anchor: TooltipStateContextDataVisible['anchor'];
}
export declare const TooltipWrapper: React.NamedExoticComponent<React.PropsWithChildren<TooltipWrapperProps>>;
export {};
//# sourceMappingURL=TooltipWrapper.d.ts.map