export declare type TooltipAnchor = 'top' | 'right' | 'bottom' | 'left' | 'center';
export declare type CrosshairType = 'x' | 'y' | 'top-left' | 'top' | 'top-right' | 'right' | 'bottom-right' | 'bottom' | 'bottom-left' | 'left' | 'cross';
//# sourceMappingURL=types.d.ts.map