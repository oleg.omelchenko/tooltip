import React from 'react';
interface CrosshairLineProps {
    x0: number;
    x1: number;
    y0: number;
    y1: number;
}
export declare const CrosshairLine: React.MemoExoticComponent<({ x0, x1, y0, y1 }: CrosshairLineProps) => JSX.Element>;
export {};
//# sourceMappingURL=CrosshairLine.d.ts.map