import { PropsWithChildren, MutableRefObject } from 'react';
interface TooltipProviderProps {
    container: MutableRefObject<HTMLDivElement>;
}
export declare const TooltipProvider: ({ container, children, }: PropsWithChildren<TooltipProviderProps>) => JSX.Element;
export {};
//# sourceMappingURL=TooltipProvider.d.ts.map