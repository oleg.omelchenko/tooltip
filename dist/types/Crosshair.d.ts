import React from 'react';
import { CrosshairType } from './types';
interface CrosshairProps {
    width: number;
    height: number;
    type: CrosshairType;
    x: number;
    y: number;
}
export declare const Crosshair: React.MemoExoticComponent<({ width, height, type, x, y }: CrosshairProps) => JSX.Element>;
export {};
//# sourceMappingURL=Crosshair.d.ts.map